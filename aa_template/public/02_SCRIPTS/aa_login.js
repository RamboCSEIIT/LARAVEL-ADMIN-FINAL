$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
    $(".preloader").fadeOut();
    // ==============================================================
    // Login and Recover Password
    // ==============================================================
    $('#to-recover').on("click", function() {
        $("#loginform").slideUp();
        $("#recoverform").fadeIn();
    });
    $('#to-login').click(function() {
        $("#recoverform").hide();
        $("#loginform").fadeIn();
    });
});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhY2thZ2UuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBIiwiZmlsZSI6ImFhX2xvZ2luLmpzIiwic291cmNlc0NvbnRlbnQiOlsiJCggZG9jdW1lbnQgKS5yZWFkeShmdW5jdGlvbigpIHtcbiAgICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykudG9vbHRpcCgpO1xuICAgICQoXCIucHJlbG9hZGVyXCIpLmZhZGVPdXQoKTtcbi8vID09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09XG4vLyBMb2dpbiBhbmQgUmVjb3ZlciBQYXNzd29yZFxuLy8gPT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT09PT1cbiAgICAkKCcjdG8tcmVjb3ZlcicpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24oKSB7XG4gICAgICAgICQoXCIjbG9naW5mb3JtXCIpLnNsaWRlVXAoKTtcbiAgICAgICAgJChcIiNyZWNvdmVyZm9ybVwiKS5mYWRlSW4oKTtcbiAgICB9KTtcbiAgICAkKCcjdG8tbG9naW4nKS5jbGljayhmdW5jdGlvbigpe1xuXG4gICAgICAgICQoXCIjcmVjb3ZlcmZvcm1cIikuaGlkZSgpO1xuICAgICAgICAkKFwiI2xvZ2luZm9ybVwiKS5mYWRlSW4oKTtcbiAgICB9KTtcbn0pO1xuXG4iXX0=
