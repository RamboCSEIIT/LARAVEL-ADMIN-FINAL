<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
 



Route::match(['get','post'],'/admin-login','AA_LOGIN\index@_aa_login');

//Route::get("/admin-login","AA_LOGIN\index@_aa_login");

Auth::routes();


Route::get('/home', 'HomeController@index')->name('home');


Route::get("/admin/dash_board","AB_ADMIN_PANEL\index@_ab_admin_panel");

