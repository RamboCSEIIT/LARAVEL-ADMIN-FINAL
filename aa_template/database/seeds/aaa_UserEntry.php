<?php

use Illuminate\Database\Seeder;

class aaa_UserEntry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $factory=factory(App\User::class,10);
        $factory->create();
        $userData = array(        'name' => 'James',
            'email' => 'a@a.com',
            'password' => bcrypt("1111"),
            'remember_token' => str_random(10),
            'admin'=>true
        );
        App\User::create($userData);
    }
}
