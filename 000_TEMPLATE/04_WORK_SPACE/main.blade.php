@extends('<%=DNAME%>.base')

@section('title')
    Test
@endsection

@section('cssBlock')
    @include('<%=DNAME%>.aa_include.za_css')
@endsection

@section('content')


    <div class="  container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @include('<%=DNAME%>.aa_include.zc_navbar')
            </div>

        </div>

    </div>




    <br>
    <br>
    @include('zz_body.<%=DNAME%>.body')




@endsection


@section('bottomJS')

    @include('<%=DNAME%>.aa_include.zb_javascript')

@endsection


 
  