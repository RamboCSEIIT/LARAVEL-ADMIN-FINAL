@extends('AB_ADMIN_PANEL.base')

@section('title')
    Test
@endsection

@section('cssBlock')
    @include('AB_ADMIN_PANEL.aa_include.za_css')
@endsection

@section('content')

    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">

    @include('zz_body.AB_ADMIN_PANEL.body')

    </div>


@endsection


@section('bottomJS')

    @include('AB_ADMIN_PANEL.aa_include.zb_javascript')

@endsection


 
  